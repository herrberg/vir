from semopy.stats import calculate_likelihood, calculate_p_values
from semopy.utils import get_cv_data_ann_kfold, cov
from pandas import read_csv, concat, Series
from semopy import Model, Optimizer
from string import ascii_lowercase
from itertools import product
from numpy import mean
import pickle


num_pcs = 1
num_mod_pcs = 8
maxnum = 10000  # Max number of SNPs to iterate over at first stage.
pcs_mod = ['PC{}'.format(i) for i in range(1, num_mod_pcs + 1)]
pcs = pcs_mod[:num_pcs]  # ['PC1']
filename_postfix = ''
models_dir = 'mods'
gwas_dir = 'gwas-pca'
data_file = 'data.txt'
snps_file = 'snps.txt'
data = read_csv(data_file, engine='c', index_col=0, low_memory=False)
snps_data = read_csv(snps_file, engine='c', index_col=0, low_memory=False)
snps_data = snps_data.T.drop_duplicates().T
data = concat([data, snps_data], axis=1, join_axes=[data.index])
data = (data - data.mean()) / data.std()
with open('{}/mod35_a.txt'.format(models_dir), 'r') as f:
    traits_corr = read_csv('trait_name_corresp.txt', sep='\t', header=None)
    traits_d = Series(traits_corr[1].values, index=traits_corr[0]).to_dict()
    for pre, pc in product(ascii_lowercase[:8], pcs_mod):
        traits_d[pre + pc] = pc
    mod = f.read()
    for t in traits_d:
        mod = mod.replace(t, traits_d[t])


def calculate_mlr(mod: str, data, force_load=set()):
    model = Model(mod, force_load=force_load)
    model.load_dataset(data)
    opt = Optimizer(model)
    return opt.optimize()


def prepare_cv_data(data, its=10):
    return [get_cv_data_ann_kfold(data, 3) for _ in range(its)]


def calculate_cv_mlr(mod: str, force_load, datas):
    means = list()
    for data in datas:
        train, tests = data
        model = Model(mod, force_load=force_load)
        model.load_dataset(train)
        opt = Optimizer(model)
        opt.optimize()
        mls = list()
        for test in tests:
            test = test[model.vars['IndsObs']]
            opt.mx_cov = cov(test.values)
            mls.append(opt.ml_wishart(opt.params))
        means.append(mean(mls))
    return mean(means)


def test_pc(base_model: str, pc: str, data, maxnum):
    global gwas_dir
    global snps_data
    gwas_filename = '{}/{}.txt'.format(gwas_dir, pc)
    gwas_data = read_csv(gwas_filename, index_col=0,
                         low_memory=False).loc[snps_data.columns]
    gwas_data = gwas_data.dropna().sort_values(['adj_pc'])
    res = list()
    for i, (snp, row) in enumerate(gwas_data.iterrows()):
        if i > maxnum:
            break
        pval_gwas = row['adj_pc']
        mod = '{}\n{} ~ {}'.format(base_model, pc, snp)
        mod_base = '{}\n{} ~~ {}'.format(base_model, pc, snp)
        mlr_base = calculate_mlr(mod_base, data, {snp})
        model = Model(mod)
        model.load_dataset(data)
        opt = Optimizer(model)
        mlr = opt.optimize()
        pvals = calculate_p_values(opt)
        m, n = model.beta_names[0].index(pc), model.beta_names[1].index(snp)
        k = model.parameters['Beta'].index(tuple((m, n)))
        pval = pvals[k]
        val = opt.params[k]
        diff_mlr = mlr_base - mlr  # The smaller MLR the better
        print('{:>12}: {:.4f}, {:.4f}, {:.4f}'.format(snp, diff_mlr, val, pval))
        print('Status: {:.2f}%'.format((i + 1) / gwas_data.shape[0] * 100))
        res.append(tuple((snp, diff_mlr, val, pval, i, pval_gwas)))
    res = sorted(res, key=lambda x: -x[1])
    return res


def test_pc_comb(base_model: str, pc, data, d, maxnum=20):
    infos = d[pc]
    mod = base_model
    ret = list()
    for i in infos:
        mod_tmp = mod + '\n{} ~ {}'.format(pc, i[0])
        mlr_base = calculate_mlr(mod, data, {i[0]})
        mlr = calculate_mlr(mod_tmp, data)
        print('Base MLR: {:.4f}, Conn MLR: {:.4f}, SNP: {:>12}'.format(mlr_base, mlr, i[0]))
        print('{}/{}'.format(len(ret) + 1, maxnum))
        if mlr < mlr_base:
            mod = mod_tmp
            ret.append(i[0])
            if len(ret) >= maxnum:
                break
    return ret


def d_to_file(d, filename):
    with open(filename, 'w') as f:
        for pc in d:
            f.write(pc + ':\n')
            f.write('{:>18}\t{:>10}\t{:>10}\t{:>10}\t{:>9}\t{:>9}\n'.format('SNP', 'diff (MLR)', 'val', 'pval', 'GWAS pos', 'GWAS pval'))
            for i in d[pc]:
                s = '{:>18}\t{:>10.4f}\t{:>10.4f}\t{:>10.4f}\t{:>9}\t{:>10.4f}\n'.format(*i)
                f.write(s)
    pickle.dump(d, open('pkl'+filename, 'wb'))    


# "First stage" - test first maxnum SNPs for each PC.
d = dict()
for pc in pcs:
    print(pc)
    t = test_pc(mod, pc, data, maxnum)
    d[pc] = t
d_to_file(d, 'out' + filename_postfix + '.txt')


# d_fit will contain all stat. significant SNPs that cause an improvement on
# MLR statistics.
d_fit = dict()
for pc in d:
    d_fit[pc] = list()
    for entry in d[pc]:
        if entry[1] > 0 and entry[-3] < 5e-2:
            d_fit[pc].append(entry)

d_to_file(d_fit, 'out_sgn' + filename_postfix + '.txt')


# The "second stage" - we iterate over snps from d_fit for each PC while trying
# to add them in order in a such way that MLR improves.
# Extremely slow in case of high SNPs number.
with open('out_comb' + filename_postfix + '.txt', 'w') as f:
    res_d = dict()
    for pc in pcs:
        if d[pc]:
            print('{}:'.format(pc))
            f.write('{}:\n'.format(pc))
            res_d[pc] = test_pc_comb(mod, pc, data, d_fit)
            f.write(', '.join(res_d[pc]) + '\n')


#import matplotlib.pyplot as plt
#
#plt.figure()
#pcs = ['PC1', 'PC2', 'PC3', 'PC4', 'PC5', 'PC6', 'PC7', 'PC8']
#for n, pc in enumerate(pcs):
#    x, y = list(), list()
#    t = sorted(d[pc], key=lambda x: x[-2])
#    for i in t:
#        x.append(i[-2])
#        y.append(i[2])
#    plt.subplot(2, 4, n + 1)
#    plt.plot(x, y)
#    plt.title(pc)
#    plt.xlabel('GWAS pos')
#    plt.ylabel('MLR difference')

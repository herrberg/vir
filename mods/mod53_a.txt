gPC1 ~ bPC6 + cPC4 + ePC8
hPC3 ~ cPC4 + fPC5
fPC5 ~ cPC4
bPC6 ~ aPC2
dPC7 ~ cPC4
ePC8 ~ dPC7
gPC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
aPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
hPC3 =~ BegFEndF + EndFBegM
cPC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
fPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
bPC6 =~ BegMEndM + StemL1ord
dPC7 =~ GBegF + BegMEndM + StemBr + PodD
ePC8 =~ BegMEndM + StemL1ord + PodS + PodShape

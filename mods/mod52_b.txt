fPC1 ~ dPC8
gPC2 ~ aPC6 + bPC5 + cPC7 + dPC8 + ePC3 + fPC1
ePC3 ~ aPC6
hPC4 ~ dPC8 + fPC1
cPC7 ~ aPC6 + bPC5
dPC8 ~ bPC5 + cPC7
fPC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
gPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
ePC3 =~ BegFEndF + EndFBegM
hPC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
bPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
aPC6 =~ BegMEndM + StemL1ord
cPC7 =~ GBegF + BegMEndM + StemBr + PodD
dPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

cPC1 ~ bPC7
gPC4 ~ aPC2 + cPC1
hPC8 ~ ePC6
cPC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
aPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
fPC3 =~ BegFEndF + EndFBegM
gPC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
dPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
ePC6 =~ BegMEndM + StemL1ord
bPC7 =~ GBegF + BegMEndM + StemBr + PodD
hPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

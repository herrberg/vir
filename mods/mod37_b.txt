ePC1 ~ aPC6 + cPC4 + dPC8
fPC2 ~ aPC6 + bPC7 + dPC8 + ePC1
gPC3 ~ bPC7 + cPC4 + ePC1
cPC4 ~ bPC7
hPC5 ~ bPC7 + cPC4 + ePC1
dPC8 ~ bPC7 + cPC4
ePC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
fPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
gPC3 =~ BegFEndF + EndFBegM
cPC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
hPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
aPC6 =~ BegMEndM + StemL1ord
bPC7 =~ GBegF + BegMEndM + StemBr + PodD
dPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

hPC1 ~ aPC3 + cPC8 + ePC4 + gPC6
ePC4 ~ bPC2 + cPC8
fPC5 ~ aPC3 + cPC8 + dPC7
dPC7 ~ aPC3 + cPC8
hPC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
bPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
aPC3 =~ BegFEndF + EndFBegM
ePC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
fPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
gPC6 =~ BegMEndM + StemL1ord
dPC7 =~ GBegF + BegMEndM + StemBr + PodD
cPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

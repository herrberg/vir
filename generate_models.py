from semopy import StructureAnalyzer, Model, visualize
from pandas import read_csv
from random import shuffle
from numpy import where
# Generates models.


def get_mod():
    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    shuffle(letters)
    pcs = "{}PC1,{}PC2,{}PC3,{}PC4,{}PC5,{}PC6,{}PC7,{}PC8".format(*letters)
    pcs = pcs.split(',')
    mod = """
    {} =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W 
    {} =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight 
    {} =~ BegFEndF + EndFBegM 
    {} =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape 
    {} =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord 
    {} =~ BegMEndM + StemL1ord 
    {} =~ GBegF + BegMEndM + StemBr + PodD 
    {} =~ BegMEndM + StemL1ord + PodS + PodShape
    """.format(*pcs)
    return mod, pcs


n = 100
models_dir = 'mods'
data_file = 'data.txt'
data = read_csv(data_file, engine='c', na_filter=False, index_col=None,
                low_memory=False)

for i in range(1, n + 1):
    print("Shuffle {}".format(i))
    mod, _ = get_mod()
    sa = StructureAnalyzer([], mod, data)
    n, lfs,  _, ml_means, _, num_pvals, conns, descs = sa.analyze(True)
    stat_sgn = where((num_pvals == 0) & (lfs < 7.5))[0]
    if not len(stat_sgn):
        continue
    k = min(n[stat_sgn], key=lambda x: ml_means[x])
    mod_a = descs[k]
    m = Model(mod_a)
    title = 'Model {}(A)\nMLR-CV: {:.4f}, MLR-full: {:.4f}, N: {}'.format(i, ml_means[k], lfs[k], k)
    filename = '{}/mod{}_a'.format(models_dir, i)
    visualize(m, view=False, filename=filename, title=title)
    with open(filename + '.txt', 'w') as f:
        f.write(mod_a)
    k = min(n[stat_sgn], key=lambda x: lfs[x])
    mod_b = descs[k]
    m = Model(mod_b)
    title = 'Model {}(B)\nMLR-CV: {:.4f}, MLR-full: {:.4f}, N: {}'.format(i, ml_means[k], lfs[k], k)
    filename = '{}/mod{}_b'.format(models_dir, i)
    visualize(m, view=False, filename=filename, title=title)
    with open(filename + '.txt', 'w') as f:
        f.write(mod_b)

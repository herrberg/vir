fPC1 ~ ePC4
hPC2 ~ ePC4 + fPC1
bPC7 ~ aPC6
cPC8 ~ aPC6
fPC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
hPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
dPC3 =~ BegFEndF + EndFBegM
ePC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
gPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
aPC6 =~ BegMEndM + StemL1ord
bPC7 =~ GBegF + BegMEndM + StemBr + PodD
cPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

cPC1 ~ aPC3 + bPC7
gPC2 ~ cPC1 + fPC4
fPC4 ~ cPC1
dPC5 ~ bPC7 + cPC1
bPC7 ~ aPC3
hPC8 ~ bPC7 + cPC1
cPC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
gPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
aPC3 =~ BegFEndF + EndFBegM
fPC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
dPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
ePC6 =~ BegMEndM + StemL1ord
bPC7 =~ GBegF + BegMEndM + StemBr + PodD
hPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

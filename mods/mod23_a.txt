hPC1 ~ bPC6 + dPC8
fPC2 ~ bPC6 + dPC8
ePC3 ~ cPC7
gPC4 ~ fPC2
hPC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
fPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
ePC3 =~ BegFEndF + EndFBegM
gPC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
aPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
bPC6 =~ BegMEndM + StemL1ord
cPC7 =~ GBegF + BegMEndM + StemBr + PodD
dPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

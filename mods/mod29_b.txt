ePC1 ~ bPC6 + cPC3
cPC3 ~ aPC2 + bPC6
hPC4 ~ aPC2 + bPC6 + ePC1 + gPC8
bPC6 ~ aPC2
dPC7 ~ bPC6
gPC8 ~ aPC2 + bPC6 + cPC3 + ePC1
ePC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
aPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
cPC3 =~ BegFEndF + EndFBegM
hPC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
fPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
bPC6 =~ BegMEndM + StemL1ord
dPC7 =~ GBegF + BegMEndM + StemBr + PodD
gPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

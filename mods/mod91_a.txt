ePC2 ~ dPC7
hPC3 ~ dPC7
fPC4 ~ bPC8 + ePC2
gPC6 ~ bPC8
dPC7 ~ bPC8
aPC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
ePC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
hPC3 =~ BegFEndF + EndFBegM
fPC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
cPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
gPC6 =~ BegMEndM + StemL1ord
dPC7 =~ GBegF + BegMEndM + StemBr + PodD
bPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

dPC1 ~ aPC6
ePC4 ~ bPC2 + dPC1
fPC5 ~ aPC6 + cPC7
cPC7 ~ aPC6
gPC8 ~ aPC6 + dPC1
dPC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
bPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
hPC3 =~ BegFEndF + EndFBegM
ePC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
fPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
aPC6 =~ BegMEndM + StemL1ord
cPC7 =~ GBegF + BegMEndM + StemBr + PodD
gPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

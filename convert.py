from pandas import read_csv, Series
# I've used it to convert names to a proper usable form in data files.
# No need to run it anymore as data is already good.


def mapper1(x):
    if 'vir_' not in x:
        return 'vir_{:0>4}'.format(x)

def mapper2(x):
    x = x[:x.index('_')]
    return x

data = read_csv('data.txt', sep='\t', engine='c', low_memory=False)
ind_corr = read_csv('sample_name_corresp.txt', sep='\t', header=None)
ind_d = Series(ind_corr[1].values, index=ind_corr[0]).to_dict()
if 'vir_' not in data['VIRCat'][0]:
    for i, v in enumerate(data['VIRCat']):
        data['VIRCat'][i] = 'vir_{:0>4}'.format(v)
data.to_csv('data.txt', index=False)


data_snp = read_csv('snps.txt', sep='\t', engine='c', index_col=0,
                    low_memory=False)
data_snp = data_snp.rename(index=ind_d, columns=mapper2)
data_snp.to_csv('snps.txt')


gwas_dir = 'gwas-pca'
num_pcs = 8
for i in range(1, num_pcs + 1):
    filename = '{}/PC{}.txt'.format(gwas_dir, i)
    with open(filename, 'r') as f:
        txt = f.readlines()
    for i, line in enumerate(txt):
        if str.isdigit(line[0]):
            txt[i] = 'Ca' + txt[i]
    with open(filename, 'w') as f:
        f.writelines(txt)
#    p = read_csv(filename, sep=',',index_col=None, low_memory=False)
#    for i, v in enumerate(p['snp']):
#        p['snp'][i] = 'Ca' + p['snp'][i]
#    p.to_csv(filename, index=False)
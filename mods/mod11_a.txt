fPC4 ~ cPC2 + dPC8
gPC6 ~ bPC1 + fPC4
ePC7 ~ bPC1
dPC8 ~ bPC1
bPC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
cPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
aPC3 =~ BegFEndF + EndFBegM
fPC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
hPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
gPC6 =~ BegMEndM + StemL1ord
ePC7 =~ GBegF + BegMEndM + StemBr + PodD
dPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

ePC1 ~ aPC3 + dPC8
gPC2 ~ aPC3 + dPC8 + ePC1
hPC4 ~ dPC8 + gPC2
bPC5 ~ aPC3
fPC7 ~ ePC1
dPC8 ~ aPC3
ePC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
gPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
aPC3 =~ BegFEndF + EndFBegM
hPC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
bPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
cPC6 =~ BegMEndM + StemL1ord
fPC7 =~ GBegF + BegMEndM + StemBr + PodD
dPC8 =~ BegMEndM + StemL1ord + PodS + PodShape

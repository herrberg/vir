dPC2 ~ bPC1 + cPC6
ePC4 ~ aPC3 + cPC6 + dPC2
cPC6 ~ bPC1
hPC7 ~ cPC6 + ePC4
fPC8 ~ cPC6
bPC1 =~ FloCol + StemCol + LeafSize + FlowStemCol + NoPodsWeight + PodLength + PodWidth + SeedCol + Seed1000W
dPC2 =~ PodsWeight + PodsNumber + SeedsNumber + SeedsWeight
aPC3 =~ BegFEndF + EndFBegM
ePC4 =~ Germ + BushShape + AscoRes + StemBr + Height + Hlp + SeedShape
gPC5 =~ BegFEndF + StemBr + StemBr1ord + StemBr2ord
cPC6 =~ BegMEndM + StemL1ord
hPC7 =~ GBegF + BegMEndM + StemBr + PodD
fPC8 =~ BegMEndM + StemL1ord + PodS + PodShape
